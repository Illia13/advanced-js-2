"use strict";

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

function createBookList(books) {
  books.forEach((book, index) => {
    try {
      const { author, name, price } = book;

      if (!author) {
        throw new Error(
          `Помилка в об'єкті на індексі ${index}: відсутня властивість "author"`
        );
      }
      if (!name) {
        throw new Error(
          `Помилка в об'єкті на індексі ${index}: відсутня властивість "name"`
        );
      }
      if (typeof price !== "number") {
        throw new Error(
          `Помилка в об'єкті на індексі ${index}: відсутня властивість "price" або значення не є числом`
        );
      }
      const ul = document.createElement("ul");
      const li = document.createElement("li");
      li.textContent = `Автор: ${author}, Назва: ${name}, Ціна: ${price} грн`;
      ul.appendChild(li);
    } catch (error) {
      console.error(error.message);
    }
  });

  return ul;
}

document.addEventListener("DOMContentLoaded", () => {
  const root = document.getElementById("root");
  const bookList = createBookList(books);
  root.appendChild(bookList);
});
